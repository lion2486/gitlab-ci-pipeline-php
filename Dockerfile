ARG DOCKER_TAG

FROM edbizarro/gitlab-ci-pipeline-php:${DOCKER_TAG}

USER root

RUN apk add --update docker
RUN addgroup php docker

USER php

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer
